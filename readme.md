Google microservices-demo
------
#### I used microservices-demo by google to train my DevOPS skills and learn more about HELM and Kubernetes!


Setup:
Clone this project and enter open the folder
```
git pull https://gitlab.com/tcpgrim/google-microservices-demo.git
cd google-microservices-demo
```
It is recommended to create and set a custom namespace
```
kubectl create namespace microservices-demo
```
Install with a simle helmfile command
```
helmfile sync -n microservices-demo
```
Check if everything okay with kubectl
```
kubectl get all -n microservices-demo
```